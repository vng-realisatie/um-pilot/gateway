FROM kong:2.6.0-alpine

USER root

ENV KONG_PLUGINS=oidc,cors

COPY ./conf/kong/kong.compose.conf /etc/kong/kong.conf
COPY ./conf/gateway/kong.compose.yml /usr/local/kong/declarative/kong.yml

RUN chown kong:root /etc/kong/kong.conf
RUN chown kong:root /usr/local/kong/declarative/kong.yml

# install kong-oidc plugin from https://github.com/nokia/kong-oidc/
RUN luarocks install kong-oidc

USER kong
